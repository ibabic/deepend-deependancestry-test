﻿using Deepend.DeependAncestry.Data.Models;
using Deepend.DeependAncestry.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Deepend.DeependAncestry.Business.Services
{
    public class AncestryDataService
    {
        private const int _NumberOfPeopleToGet = 10;
        private const int _ItemsPerPage = 10;

        void GetAncestors(AncestryDataModel adm, PersonModel descendant, string gender, ref List<AncestryDataResultModel> results)
        {
            var query = adm.people.Where(x => x.id == descendant.father_id || x.id == descendant.mother_id);

            if (query.Count() > 0)
            {
                results.AddRange(query.Take(_NumberOfPeopleToGet - results.Count).Select(x => new AncestryDataResultModel()
                {
                    id = x.id,
                    gender = x.gender == "M" ? "Male" : "Female",
                    name = x.name,
                    level = x.level,
                    place_id = x.place_id,
                    place_name = adm.places.Where(p => p.id == x.place_id).SingleOrDefault().name
                }).ToList());

                if (results.Count < _NumberOfPeopleToGet + 1)
                {
                    GetAncestors(adm, query.Where(x => x.gender == gender).SingleOrDefault(), gender, ref results);

                }
            }
        }

        void GetDescendants(AncestryDataModel adm, PersonModel ancestor, ref List<AncestryDataResultModel> results)
        {
            var query = adm.people.Where(x => x.father_id == ancestor.id || x.mother_id == ancestor.id);

            if (query.Count() > 0)
            {
                results.AddRange(query.Take(_NumberOfPeopleToGet - results.Count).Select(x => new AncestryDataResultModel()
                {
                    id = x.id,
                    gender = x.gender == "M" ? "Male" : "Female",
                    name = x.name,
                    level = x.level,
                    place_id = x.place_id,
                    place_name = adm.places.Where(p => p.id == x.place_id).SingleOrDefault().name
                }).ToList());

                if (results.Count != _NumberOfPeopleToGet)
                {
                    foreach (var item in query)
                    {
                        GetDescendants(adm, item, ref results);
                    }
                }

            }
        }


        public AncestryDataPagingModel GetAdvancedSearchResults(string name, string gender, string direction)
        {
            AncestryData ancestryData = new AncestryData();
            AncestryDataModel adm = ancestryData.LoadData();
            AncestryDataPagingModel model = new AncestryDataPagingModel();

            List<AncestryDataResultModel> results = new List<AncestryDataResultModel>();

            PersonModel searchPerson = adm.people.Where(x => x.name.ToLower() == name.ToLower().Trim() && x.gender == gender).OrderByDescending(x => x.id).Take(1).SingleOrDefault();

            switch (direction)
            {
                case "Ancestors":
                    GetAncestors(adm, searchPerson, gender, ref results);
                    model.Results = results;

                    return model;

                case "Descendants":
                    GetDescendants(adm, searchPerson, ref results);
                    model.Results = results;

                    return model;
                default:
                    return model;
            }

        }


        public AncestryDataPagingModel GetSearchResults(string name, string gender, int pageNumber)
        {
            AncestryData ancestryData = new AncestryData();
            AncestryDataModel adm = ancestryData.LoadData();
             
            IEnumerable<PersonModel> query = null;
            AncestryDataPagingModel model = new AncestryDataPagingModel();

            query = adm.people.Where(x => x.name.ToLower().Contains(name.ToLower().Trim()));

            if (!string.IsNullOrEmpty(gender))
            {
                query = query.Where(x => x.gender == gender);
            }

            model.Results = query.Skip(pageNumber * (int)_ItemsPerPage).Take((int)_ItemsPerPage).Select(x => new AncestryDataResultModel()
            {
                id = x.id,
                gender = x.gender == "M" ? "Male" : "Female",
                name = x.name,
                level = x.level,
                place_id = x.place_id,
                place_name = adm.places.Where(p => p.id == x.place_id).SingleOrDefault().name
            }).ToList();

            model.CurrentPageNumber = pageNumber;
            model.TotalItemCount = query.Count();
            model.NumberOfPages = Math.Ceiling(model.TotalItemCount / _ItemsPerPage);

            return model;
        }
    }
}
