﻿using System.ComponentModel.DataAnnotations;

namespace Deepend.DeependAncestry.Web.Models
{
    public class SearchFormModel
    {
        [Required]
        [Display(Name = "Name")]
        public string name { get; set; }

        [Display(Name = "Male")]
        public bool M { get; set; }

        [Display(Name = "Female")]
        public bool F { get; set; }
        public string Direction { get; set; }
    }
}
