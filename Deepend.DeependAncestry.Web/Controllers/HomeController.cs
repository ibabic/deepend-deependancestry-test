﻿using Deepend.DeependAncestry.Business.Services;
using Deepend.DeependAncestry.Web.Models;
using System.Web.Mvc;

namespace Deepend.DeependAncestry.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public PartialViewResult GetSearchResults(SearchFormModel model, int pageNumber)
        {
            AncestryDataService ads = new AncestryDataService();

            return PartialView("_ResultsTable", ads.GetSearchResults(model.name.ToLower(), model.M && model.F ? string.Empty : model.M ? "M" : model.F ? "F" : string.Empty, pageNumber));
        }

    }
}