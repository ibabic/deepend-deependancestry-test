﻿using Deepend.DeependAncestry.Business.Services;
using Deepend.DeependAncestry.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Deepend.DeependAncestry.Web.Controllers
{
    public class AdvancedController : Controller
    {
        // GET: Advanced
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public PartialViewResult GetSearchResults(SearchFormModel model)
        {
            AncestryDataService ads = new AncestryDataService();

            return PartialView("_ResultsTable", ads.GetAdvancedSearchResults(model.name.ToLower(), model.M && model.F ? "M" : model.M ? "M" : model.F ? "F" : "M", model.Direction)); // default Male

        }

    }
}