﻿using Deepend.DeependAncestry.Data.Models;
using Newtonsoft.Json;
using System.IO;
using System.Web;

namespace Deepend.DeependAncestry.Data.Repository
{
    public class AncestryData
    {
        public AncestryDataModel LoadData()
        {
            //load data from the json file specified. This can be changed so that the file name is a property in web.config
            //this could also be cached to prevent constant loading from disk
            using (StreamReader r = new StreamReader(HttpContext.Current.Server.MapPath(@"\App_Data\data_large.json")))
            {
                string json = r.ReadToEnd();
                AncestryDataModel items = JsonConvert.DeserializeObject<AncestryDataModel>(json);
                return items;
            }

        }
    }
}
