﻿namespace Deepend.DeependAncestry.Data.Models
{
    public class AncestryDataResultModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string gender { get; set; }
        public int place_id { get; set; }
        public string place_name { get; set; }
        public int level { get; set; }

    }
}
