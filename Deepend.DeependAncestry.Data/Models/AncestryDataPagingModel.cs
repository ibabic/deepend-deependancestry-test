﻿using System.Collections.Generic;

namespace Deepend.DeependAncestry.Data.Models
{
    public class AncestryDataPagingModel
    {
        public AncestryDataPagingModel()
        {
            Results = new List<AncestryDataResultModel>();
        }
        public List<AncestryDataResultModel> Results { get; set; }

        public int CurrentPageNumber { get; set; }

        public double NumberOfPages { get; set; }

        public double TotalItemCount { get; set; } 
    }
}
