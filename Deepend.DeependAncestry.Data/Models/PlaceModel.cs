﻿namespace Deepend.DeependAncestry.Data.Models
{
    public class PlaceModel
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
