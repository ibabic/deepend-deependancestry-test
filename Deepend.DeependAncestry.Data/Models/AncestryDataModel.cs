﻿using System.Collections.Generic;

namespace Deepend.DeependAncestry.Data.Models
{
    public class AncestryDataModel
    {
        public AncestryDataModel()
        {
            places = new List<PlaceModel>();
            people = new List<PersonModel>();
        }
        public List<PlaceModel> places { get; set; }

        public List<PersonModel> people { get; set; }
    }
}
